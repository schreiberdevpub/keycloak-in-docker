# Run Keycloak in Docker

## pipeline status

[![pipeline status](https://gitlab.com/schreiberdevpub/keycloak-in-docker/badges/main/pipeline.svg)](https://gitlab.com/schreiberdevpub/keycloak-in-docker/-/commits/main)

## introduction

Run Keycloak in a Docker container without having to read through the [documentation](https://www.keycloak.org/server/containers).
PostgreSQL is used as the database.
Please note that it is assumed that Keycloak will run behind some kind of reverse proxy
e.g. [Traefik](https://doc.traefik.io/traefik/).

## example docker-compose

```yaml
version: "3"
services:
  runtime:
    image: schreiberdev/keycloak
    restart: unless-stopped
    depends_on:
      - db
    environment:
      - KC_ADMIN=kcadmin
      - KC_ADMIN_PASSWORD=<CHANGE_ME>
      - KC_HOSTNAME=<CHANGE_ME>
      - KC_DB_URL=jdbc:postgresql://db:5432/keycloak
      - KC_DB_USERNAME=keycloak
      - KC_DB_PASSWORD=keycloak
      - TZ=Europe/Athens
  db:
    image: postgres
    environment:
      - POSTGRES_PASSWORD=keycloak
      - POSTGRES_USER=keycloak
      - POSTGRES_DB=keycloak
    restart: unless-stopped
    volumes:
      - db:/var/lib/postgresql/data
volumes:
  db:
```
