FROM quay.io/keycloak/keycloak:26.1.3 as builder

ENV KC_DB=postgres

RUN /opt/keycloak/bin/kc.sh build

FROM quay.io/keycloak/keycloak:26.1.3

COPY --from=builder /opt/keycloak/ /opt/keycloak/

WORKDIR /opt/keycloak

ENTRYPOINT ["bin/kc.sh", "start"]

CMD ["--optimized", "--proxy-headers=xforwarded", "--http-enabled=true"]
